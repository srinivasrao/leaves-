import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  AUTH_SERVER = "http://localhost:3000";

  authSubject  =  new  BehaviorSubject(false);

  constructor(private httpclient:HttpClient) { }

  signIn(user:User){
    return this.httpclient.post(`${this.AUTH_SERVER}/login`, user);
  }
}
