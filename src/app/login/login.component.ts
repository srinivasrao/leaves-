import { Component, OnInit } from '@angular/core';
import {FormControl, FormArray, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from  '../service/auth.service';
import {User} from '../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted  =  false;
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit( ) {
    this.loginForm  =  this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get formControls() { 
    return this.loginForm.controls; 
  }

  login(){
    //console.log(this.loginForm.value);
    const data = this.loginForm.value;
    console.log(data);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
     this.authService.signIn(data).subscribe((res)=>{
    //   console.log("Logged in!");
      //this.router.navigateByUrl('home');

    });    
    
  }

}
